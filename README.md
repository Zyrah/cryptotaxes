# CryptoTaxes

Skatteverket har en "hjälpblankett" som det här verktyget ersätter. Man använder resultatet på samma sätt som med hjälpblanketten, finns en guide hur det går till här: https://bt.cx/sv/news/2017/03/30/deklarationsdags-angest-har-du-salt-bitcoin/

## Instruktioner
Generera en CSV-rapport från coinbase pro för året som ska deklareras och lägg CSV-filen i resorces byt ut sökvägen i Declarer.java så att den matcher filnamnet på CSV-filen. OBS. om CSV-filer från andra mäklare ska tolkas kan kalkylerna behöva korrigeras, coinbase pro genererar exempelvis priset för en transaktion med minus.

Byt ut columnnamnen i CSV-filen till respektive fältnamn i Order.java. Börja columnnamnen med stor bokstav så att de recursiva uppslagen hittar set-metoderna.

Kör Declarer.java och kika på resultatet i resources/result.csv

Kika igenom resultatet så att det stämmer