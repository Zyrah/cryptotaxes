package se.declare.crypto.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Order {
	LocalDateTime createdAt;
	String side;
	Double size = 0d;
	Double totalSize = 0d;
	Double price = 0d;
	Double fee = 0d;
	Double total = 0d;
	Double totalCost = 0d;
	Double averageTaxBasisForRemainingCrypto = 0d;
	Double averageBuyCost = 0d;
	Double profit = 0d;
	
	public Order() {
		
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = LocalDateTime.parse(createdAt, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public Double getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = Double.valueOf(size);
	}
	
	public Double getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Double size) {
		this.totalSize = Double.valueOf(size);
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = Double.valueOf(price);
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = Double.valueOf(fee);
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = Double.valueOf(total);
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public Double getAverageTaxBasisForRemainingCrypto() {
		return averageTaxBasisForRemainingCrypto;
	}

	public void setAverageTaxBasisForRemainingCrypto(Double averageBuyPrice) {
		this.averageTaxBasisForRemainingCrypto = averageBuyPrice;
	}

	public Double getAverageBuyCost() {
		return averageBuyCost;
	}

	public void setAverageBuyCost(Double averageBuyCost) {
		this.averageBuyCost = averageBuyCost;
	}

	public Double getProfit() {
		return profit;
	}

	public void setProfit(Double profit) {
		this.profit = profit;
	}

	@Override
	public String toString() {
		return "\nOrder [createdAt=" + createdAt + ", side=" + side + ", size=" + size + ", totalSize=" + totalSize
				+ ", price=" + price + ", fee=" + fee + ", total=" + total + ", totalCost=" + totalCost
				+ ", averageBuyPrice=" + averageTaxBasisForRemainingCrypto + ", averageBuyCost=" + averageBuyCost +  ", profit=" + profit + "]";
	}
	
	public String toK4String(Double currencyMultiplyer) {
		return "Order [datum=" + createdAt + ", h�ndelse=" + side + ", antal=" + size + ", F�rs�ljningsbelopp=" + total * currencyMultiplyer + ", omkostnadsbelopp=" + averageBuyCost * currencyMultiplyer + ", Vinst/f�rlust=" + profit * currencyMultiplyer + "]";
	}

	public String toK4DebugString(Double currencyMultiplyer) {
		return "Order [datum=" + createdAt + ", h�ndelse=" + side + ", antal=" + size + ", F�rs�ljningsbelopp=" + total * currencyMultiplyer + ", omkostnadsbelopp=" + averageBuyCost * currencyMultiplyer + ", TotaltOmkostnadsbelopp=" + totalCost * currencyMultiplyer +  ", Vinst/f�rlust=" + profit * currencyMultiplyer + ", snittOmkostnadF�rKvarvarandeV�rdepapper=" + averageTaxBasisForRemainingCrypto * currencyMultiplyer + "]";
	}
	
	public String toOnlySellK4String(Double currencyMultiplyer) {
		return "Order [datum=" + createdAt + ", h�ndelse=" + side + ", antal=" + size + ", F�rs�ljningsbelopp=" + total * currencyMultiplyer + ", omkostnadsbelopp=" + averageBuyCost * currencyMultiplyer + ", Vinst/f�rlust=" + profit * currencyMultiplyer + "]";
	}
}
