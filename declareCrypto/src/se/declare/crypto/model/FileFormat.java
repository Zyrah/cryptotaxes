package se.declare.crypto.model;

public enum FileFormat {
	COMPLEX, SIMPLE_K4, SIMPLE_K4_ONLY_SELL, SIMPLE_K4_DEBUG
}
