package se.declare.crypto;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.declare.crypto.model.FileFormat;
import se.declare.crypto.model.Order;

public class Declarer {

	final static String FILE_PATH = "resources\\Example_report_from_GDAX_BTC_fills.csv";
	final static String OUTPUT_FILE_PATH = "resources\\result.csv";
	final static Charset ENCODING = StandardCharsets.UTF_8;
	final static Double SEK_MULTIPLYER_2018_04_26 = 10.5088;
	List <String> originalRows = new ArrayList<String>();
	List <Order> formattedOrders = new ArrayList<Order>();
	
	public List<String> readSmallTextFile(String aFileName) throws IOException {
		Path path = Paths.get(aFileName);
		originalRows = Files.readAllLines(path, ENCODING); 
		return originalRows;
	}

	public void writeSmallTextFile(String aFileName) throws IOException {
		Path path = Paths.get(aFileName);
		Files.write(path, Arrays.asList(formattedOrders.toString()), ENCODING);
	}
	
	public void printOriginalFile() throws IOException {
		System.out.println("OriginalFile: " + originalRows);
	}
	
	public void printFormattedFile(FileFormat fileFormat, Double currencyMultiplyer) {
		switch (fileFormat) {
		case SIMPLE_K4:
			formattedOrders.forEach(o -> {
				System.out.println(o.toK4String(currencyMultiplyer));
			});
			break;
		case SIMPLE_K4_DEBUG:
			formattedOrders.forEach(o -> {
				System.out.println(o.toK4DebugString(currencyMultiplyer));
			});
			break;
		case SIMPLE_K4_ONLY_SELL:
			formattedOrders.forEach(o -> {
				if ("SELL".equals(o.getSide())){
					System.out.println(o.toOnlySellK4String(currencyMultiplyer));
				}
			});
			break;
		case COMPLEX:
			System.out.println("FormattedFile: " + formattedOrders);
			break;
		}
	}
	
	public void parseFile() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		boolean firstRow = true;
		Map<Integer, String> methodNames = new HashMap<Integer, String>();
		for (String row : originalRows) {
			Order order = new Order();
			List <String> items = Arrays.asList(row.split(","));
			if (firstRow) {
				// Get columns that are interesting
				for (int i = 0; items.size() > i; i++) {
					methodNames.put(Integer.valueOf(i), items.get(i));
				}
				firstRow = false;
				continue;
			}
			// put all of the interesting columns in Order objects
			for (int i = 0; items.size() > i; i++) {
				Method method;
				try {
					method = order.getClass().getMethod("set" + methodNames.get(i), String.class);
				} catch (NoSuchMethodException e) {
					System.out.println("Skipping field: " + methodNames.get(i));
					continue;
				}
				method.invoke(order, items.get(i));
			}
			formattedOrders.add(order);
		}
	}
	
	public void calculateCosts() throws Exception {
		Double totalTaxBasis = 0D;
		Double totalSize = 0D;
		Double averageBuyPrice = 0D;
		for (Order order : formattedOrders) {
			
			if ("BUY".equals(order.getSide())) {
				totalSize += order.getSize();
				totalTaxBasis += order.getTotal();
				averageBuyPrice = totalTaxBasis / totalSize;
				if (averageBuyPrice > 0d) {
					averageBuyPrice = 0d;
				}
				
			} else if ("SELL".equals(order.getSide())){
				totalSize -= order.getSize();
				totalTaxBasis = totalSize * averageBuyPrice;
				if (totalTaxBasis > 0d) {
					totalTaxBasis = 0d;
				}
				order.setAverageBuyCost(averageBuyPrice * order.getSize());
				order.setProfit(order.getTotal() + order.getAverageBuyCost());
			} else {
				throw new Exception("Did not recognize the action of the order");
			}
			order.setTotalCost(totalTaxBasis);
			order.setAverageTaxBasisForRemainingCrypto(averageBuyPrice);
			order.setTotalSize(totalSize);
		}
	}
	
	
	
	
	
	/**
	 * Shit gets executed from here
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		Declarer declarer = new Declarer();
		declarer.readSmallTextFile(FILE_PATH);
		declarer.printOriginalFile();
		declarer.parseFile();
		declarer.calculateCosts();
		
		declarer.writeSmallTextFile(OUTPUT_FILE_PATH);
		declarer.printFormattedFile(FileFormat.SIMPLE_K4_ONLY_SELL, SEK_MULTIPLYER_2018_04_26);
	}


}
