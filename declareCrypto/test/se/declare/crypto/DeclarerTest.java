package se.declare.crypto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

import se.declare.crypto.model.FileFormat;
import se.declare.crypto.model.Order;

public class DeclarerTest {

	String FILE_PATH = "testResources\\BTC_fills.csv";
	String OUTPUT_FILE_PATH = "resources\\result.csv";
	Charset ENCODING = StandardCharsets.UTF_8;
	Double SEK_MULTIPLYER_2018_04_26 = 10.5088;
	
	@Test
	public void calculate_test_success() throws Exception {
		
		Declarer declarer = new Declarer();
		declarer.readSmallTextFile(FILE_PATH);
		declarer.printOriginalFile();
		declarer.parseFile();
		declarer.calculateCosts();
		
		declarer.printFormattedFile(FileFormat.SIMPLE_K4_DEBUG, SEK_MULTIPLYER_2018_04_26);
		
		assertTrue(declarer.formattedOrders != null);
		Order previousBuyOrder = null;
		for (Order order: declarer.formattedOrders) {
			if ("BUY".equals(order.getSide())) {
				assertTrue(order.getAverageTaxBasisForRemainingCrypto() + " != " + order.getTotalCost() / order.getTotalSize(), 
						order.getAverageTaxBasisForRemainingCrypto() == order.getTotalCost() / order.getTotalSize());
				previousBuyOrder = order;
			}
			if (previousBuyOrder != null && "SELL".equals(order.getSide())) {
				assertTrue(order.getAverageTaxBasisForRemainingCrypto() + " != " + previousBuyOrder.getTotalCost() / previousBuyOrder.getTotalSize(), 
						order.getAverageTaxBasisForRemainingCrypto() == previousBuyOrder.getTotalCost() / previousBuyOrder.getTotalSize());
			}
		}
	}
}
